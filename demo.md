Eintragen / Ändern / Löschen von A-Records
==========================================

* Startseite: Links oben erwähnen (DHCP-Leases, NATVS+. NETDOC)

* DNSVS "Ihre Bereiche": Bereich rz-netze-s0/3 auswählen
* Seite für den Bereich erklären:
  * man landet bei den A-Records
  * rechts alle Record-Typen und Bereichsinfo auswählbar
  * Bereichsinfo ist oben (wie bisher, aber für ITBs nun Domains und Betreuer bearbeitbar)
  * nach allen Spalten sortierbar => nach FQDN sortieren
  * Badges zeigen: Reserviert, Set, NATVS
  * Zeigen: Plus-Button / Freie-IP-Adressen-Platzhalter (Sonderfall für A-Records) / Bearbeiten-/Lösch-Button

* Demo: Record eintragen
  * auf freien Block Platzhalter > 1 klicken
  * erklären Felder FQDN, Record-Data und Record-Typ, FQDN-Info (inkl. M-DHCP-Logik - bleibt so)
  * Eintragen bei FQDN, FQDN-Info (Mac + Kommentar), IP-Adresse so stehen lassen

* vor dem übernehmen in Transaktion, nun Transaktion erklären
  * Liste von geplanten Aktionen
  * Änderungen in der Transaktion tauchen noch nicht in der Anzeige auf
  * bei Anwenden der Transaktion werden alle Aktionen als Transaktion auf der Datenbank ausgeführt (Reihenfolge spielt eine Rolle, ist änderbar)
  * warum? -> mittelfristig powerdns
* dann Transaktion übernehmen und anwenden

* Demo: Record ändern (den gerade eingetragenen, MAC-Adresse ändern)
  * FQDN-Info hängt am FQDN
  * um das klarzumachen, muss man auf FQDN klicken, denn es ist FQDN-Änderung
  * dort nochmal der Hinweis, dass man sich die zugehörigen Records anschauen kann
  * MAC-Adresse ändern, speichern
  * Breadcrump Navigation zeigen und wieder zum Bereich zurückgehen

* Demo: Record löschen

Alle Records zu / alle Referenzen auf FQDN
==========================================

* Wir sind im Bereich rz-netze-s0/3
  * Filterfunktion für net-web benutzen ("nun sehen wir nur noch net-web")
  * dann weiter einschränken auf "net-web07 (yeah, das is der Server, auf dem diese Webseite läuft xD)
  * alle Records zu FQDN => A und AAAA
  * alle Referenzen auf FQDN ("alles was da draufzeigt, d.h. alle Records mit dem FQDN auf der rechten Seite") => PTR und CNAME

FQDN / Domain / Betreuer eintragen
==================================

* Demo: "Ihre Bereiche". Zeigen, dass es da jetzt auch "Ihre FQDNs" gibt und dass wir das gleich brauchen zum FQDN anlegen.

* Demo: Neue Subdmomain für Projekt1 bei cs-dmz/1 eintragen (muss vorher angelegt werden) / Betreuer scc-netvs-0001 in Bereich eintragen
  * "Ihre Bereiche": Bereich cs-dmz/1 auswählen
  * Neue Domain eintragen (nur als ITB möglich): projekt1.scc.kit.edu.
  * "Oh wait: den FQDN gibt's ja noch nicht, aber cool, wir haben es ja erst in die Transaktion aufgenommen, also legen wir jetzt mal den FQDN an..."
  * erstmal aber noch den Betreuer hinzufügen (scc-netvs-test-0001)
  * "Ihre FQDNs" auswählen und "Plus" drücken, FQDN mit Typ Domain auswählen. In Transaktion übernehmen.
  * Reihenfolge in Transaktion anpassen.
  * Transaktion anwenden. Job erledigt.
  

Suchfunktion 
============

* Suchen nach radius-2 => wir sehen Records und FQDN (Records, weil uns die Bereiche gehören)
* Suchen nach radius-1 (unten weitersuchen!) => wir sehen nur den FQDN, weil uns die Bereiche nicht gehören
* darauf eingehen, was man noch ins Suchfeld eintragen kann: regexp, IP-Adresse...

